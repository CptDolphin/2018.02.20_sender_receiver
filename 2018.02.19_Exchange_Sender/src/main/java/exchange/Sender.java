package exchange;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Sender {
    private static final String FILE_PATH = "c:/users/user/ideaprojects/exchangeFile.txt";
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line;
        do {
            line = scanner.nextLine();
            System.out.println("Wysylam: " + line + "'");

            try (PrintWriter printWriter = new PrintWriter((FILE_PATH))) {
                printWriter.println(line);
                printWriter.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } while (!line.equals("quit"));
    }
}
