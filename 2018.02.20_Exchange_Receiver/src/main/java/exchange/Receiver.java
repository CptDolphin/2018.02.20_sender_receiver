package exchange;

import java.io.*;
import java.util.Scanner;

public class Receiver {
    private static final String FILE_PATH = "c:/users/user/ideaprojects/exchangeFile.txt";

    public static void main(String[] args) {
        File file = new File(FILE_PATH);
        boolean isWorking = true;

        String lastLine = null;
        long lastModified = System.currentTimeMillis();

        while (isWorking) {
            try (Scanner scanner = new Scanner(file)) {
                Thread.sleep(100);

                String line = scanner.nextLine();
                if (file.lastModified() != lastModified || (!line.equals(lastLine))) {
                    System.out.println("New line: " + line);
                    lastLine = line;
                    lastModified = file.lastModified();
                }
                if (line.equals("quit")) {
                    isWorking = false;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
